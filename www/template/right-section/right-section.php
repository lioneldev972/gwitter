<section class="right-section">


    <section class="container-search">
        <div>
            <div> <i class="fas fa-search"></i> </div>
            <div>
                <form >
                    <input  placeholder="Recherche Gwitter"  type="text"  value="" >
                </form>
            </div>
        </div>
    </section>
    <div class="container-content">
        <section class="container-grid">
            <div class="model-top model">
                <div class="top">Vous pourriez aimer</div>

            </div>
            <?php
                for ($i=0; $i<3; $i++) {

                    require "components/love.php";
                }
            ?>

            <div class="model-bottom model  ">
                <div class="bottom">Voir plus</div>
            </div>
        </section>
        <section class="container-grid ">
            <div class="model-top model">
                <div class="top">Tendances pour vous</div>
                <div class="bubble"><i class="fa fa-cog" aria-hidden="true"></i>
                </div>
            </div>
            <?php
                for ($i=0; $i<5; $i++) {

                    require "components/suggestion.php";
                }
            ?>

            <div class="model-bottom model  ">
                <div class="bottom">Voir plus</div>
            </div>
        </section>
        <section class="footer">
            <nav>
                <a href="#" class="">
                    <span class="">Conditions</span>
                </a>
                <a href="#" target="_blank" class="">
                    <span class="">Politique de confidentialité</span>
                </a>
                <a href="#" target="_blank" class="" >
                    <span class="">Cookies</span>
                </a>
                <a href="#" target="_blank" class="" >
                    <span class="">Informations sur les publicités</span>
                </a>
                <div>
                    <div>
                        Plus
                    </div>
                </div>
                <div>
                    <span class="">© 2020 Gwitter, Inc.</span>
                </div>
            </nav>
        </section>
    </div>
</section>