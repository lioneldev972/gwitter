<section class="middle-section">
    <div class="header">
        <!-- <div> -->
        <a href="">
            <div class="arrow-left">
                <i class="fas fa-arrow-left"></i>
            </div>
        </a>
        <!-- </div> -->
        <div>
            <p class="title">Albert François</p>
            <p class="sous-title">25 Tweets</p>
        </div>
    </div>
    <div class="container-content">
        <div class="content-profil">
            <div class="header-profil">

            </div>
            <div class="container-space">
                <div class="container-action-profil">
                    <div class="image-profil bubble" >
                        <img src="https://via.placeholder.com/150" alt="">
                    </div>
                    <span>
                        <button class="btn btnEdit bubble ">Editer le profil</button>
                    </span>
                </div>
                <div class="container-info">
                    <p class="title">Albert François</p>
                    <p>
                        <i class="fas fa-at"></i>
                        albert_francois</p>
                    <p>
                        <i class="fas fa-calendar-alt"></i>
                        A rejoint Gwitter en Mars 2012
                    </p>
                    <p>
                        <span><span>215</span> abonnements</span>
                        <span><span>456</span> abonnements</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="content">
            <div >
                <ul class="tabulation">
                    <li class="navtab activeTab">
                        <a href="#tweets" class="tab-link ">
                            Tweets
                        </a>
                    </li>
                    <li class="navtab">
                        <a href="#reponse" class="tab-link">
                            Tweets et réponses
                        </a>
                    </li>
                    <li class="navtab">
                        <a href="#media" class="tab-link">
                            Médias
                        </a>
                    </li>
                    <li class="navtab">
                        <a href="#love" class="tab-link">
                            J'aime
                        </a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div id="tweets" class="tab-pane activePan">
                    <div class="container-tweet">
                        <article>
                            <div class="ligne1">
                                <div><i class="fas fa-retweet"></i></div>
                                <div>Vous avez retweeté</div>
                            </div>
                            <div class="ligne2">
                                <div class="container-medaillon">
                                    <div class="bubble action_profil">
                                        <img src="https://via.placeholder.com/49" alt="">

                                    </div>
                                    <span class="card_profil"></span>
                                </div>
                                <div>
                                    <div class="other_profil">
                                                <span>
                                                    <span>Lorem Fred</span>
                                                    <span><i class="fas fa-at"></i>loren_fred</span>
                                                    <span>17 mars 2019</span>
                                                </span>
                                        <span><i class="fas fa-angle-down"></i></span>
                                    </div>
                                    <div>
                                        <div class="content-media">
                                            <div>Lorem ipsum dolor sit amet. </div>
                                            <div class="media"><img src="https://via.placeholder.com/400x300" alt=""></div>
                                        </div>
                                        <div class="container-ico">
                                            <span><i class="far fa-comment"></i>20k</span>
                                            <span><i class="fas fa-retweet"></i>5k</span>
                                            <span><i class="fas fa-heart"></i>5k</span>
                                            <span><i class="fas fa-arrow-up"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="ligne1">
                                <div><i class="fas fa-retweet"></i></div>
                                <div>Vous avez retweeté</div>
                            </div>
                            <div class="ligne2">
                                <div class="container-medaillon">
                                    <div class="bubble"><img src="https://via.placeholder.com/49" alt=""></div>
                                </div>
                                <div>
                                    <div class="other_profil">
                                                <span>
                                                    <span>Lorem Fred</span>
                                                    <span><i class="fas fa-at"></i>loren_fred</span>
                                                    <span>17 mars 2019</span>
                                                </span>
                                        <span><i class="fas fa-angle-down"></i></span>
                                    </div>
                                    <div>
                                        <div class="content-media">
                                            <div>Lorem ipsum dolor sit amet. </div>

                                        </div>
                                        <div class="container-ico">
                                            <span><i class="far fa-comment"></i>20k</span>
                                            <span><i class="fas fa-retweet"></i>5k</span>
                                            <span><i class="fas fa-heart"></i>5k</span>
                                            <span><i class="fas fa-arrow-up"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="ligne1">
                                <div><i class="fas fa-retweet"></i></div>
                                <div>Vous avez retweeté</div>
                            </div>
                            <div class="ligne2">
                                <div class="container-medaillon">
                                    <div class="bubble"><img src="https://via.placeholder.com/49" alt=""></div>
                                </div>
                                <div>
                                    <div class="other_profil">
                                                <span>
                                                    <span>Lorem Fred</span>
                                                    <span><i class="fas fa-at"></i>loren_fred</span>
                                                    <span>17 mars 2019</span>
                                                </span>
                                        <span><i class="fas fa-angle-down"></i></span>
                                    </div>
                                    <div>
                                        <div class="content-media">
                                            <div>Lorem ipsum dolor sit amet. </div>
                                            <div class="media"><img src="https://via.placeholder.com/400x300/584" alt=""></div>
                                        </div>
                                        <div class="container-ico">
                                            <span><i class="far fa-comment"></i>20k</span>
                                            <span><i class="fas fa-retweet"></i>5k</span>
                                            <span><i class="fas fa-heart"></i>5k</span>
                                            <span><i class="fas fa-arrow-up"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div id="reponse" class="tab-pane">c'est le pan 2</div>
                <div id="media" class="tab-pane">c'est le pan 3</div>
                <div id="love" class="tab-pane">c'est le pan 4</div>
            </div>
        </div>
    </div>
</section>