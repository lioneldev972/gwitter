<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="assets/Stylesheet/css/index.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<title>Gwitter</title>
</head>
<body>
	<div class="card_profil">
		<?php require "template/components/card_profil.php"; ?>
	</div>
	<section class="container">
        <?php
            require "template/left-section/left-section.php";
            require "template/middle-section/middle-section.php";
            require "template/right-section/right-section.php";
        ?>
	</section>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="assets/js/tab.js"></script>
    <script src="assets/js/card.js"></script>
</body>
</html>