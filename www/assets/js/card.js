let action_profil = document.querySelectorAll(".action_profil")
let card_profil = document.querySelector(".card_profil")
let timer

document.addEventListener('DOMContentLoaded', function(){

    for (var i = 0; i < action_profil.length; i++) {
        action_profil[i].addEventListener('mouseenter',showCardProfileModal);
        action_profil[i].addEventListener('mouseleave',hideCardProfileModal);
    }

    card_profil.addEventListener('mouseenter',showCardProfileModal)
    card_profil.addEventListener('mouseenter',hideCardProfileModal)



    function showCardProfileModal(e){
        clearTimeout(timer); 
        if(e.target != card_profil){
            const { top, right
            } = getModalPosition(e.target)
           card_profil.style.top = top + 'px'
           card_profil.style.right = right + 'px'
        }       
        
        card_profil.classList.add('visible')
    }

    function hideCardProfileModal(){

        timer = setTimeout(function(){
            card_profil.classList.remove('visible')
            console.log('set' + timer)
        }, (1500));

        console.log('set' + timer)
       
    }

})

function getModalPosition(element) {
    const circleHeight = element.getBoundingClientRect().height
   
    const actionProfileTop = element.getBoundingClientRect().y
    const actionProfileRight = element.getBoundingClientRect().x
    console.log(actionProfileRight)
    return {
        top: actionProfileTop + circleHeight + 10,
        right: -actionProfileRight
    }
}