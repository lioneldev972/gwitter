$(document).ready(function() {
    
    $("ul.tabulation li a").click(function() {
        $("ul.tabulation li").removeClass('activeTab')
        $(".tab-pane").removeClass("activePan");
        
        $(this).parent().addClass("activeTab")
        $($(this).attr("href")).addClass("activePan");
    });
})